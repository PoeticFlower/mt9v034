@echo off
if "%VS120COMNTOOLS%" == "" (
  msg "%username%" "Visual Studio 12 not detected"
  exit 1
)
if not exist MT9V034.sln (
  call make-solutions.bat
)
if exist MT9V034.sln (
  call "%VS120COMNTOOLS%\..\..\VC\vcvarsall.bat"
  MSBuild /property:Configuration="Release"        MT9V034.sln
  MSBuild /property:Configuration="Debug"          MT9V034.sln
  MSBuild /property:Configuration="RelWithDebInfo" MT9V034.sln
  MSBuild /property:Configuration="MinSizeRel"     MT9V034.sln
)
