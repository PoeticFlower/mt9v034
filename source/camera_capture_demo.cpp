/*****************************************************************************
 *
 * Copyright (c) 2015-2016, PoeticFlower Studio.
 *
 * AUTHOR  : Peixuan Zhang <Zhang@PoeticFlower.CN>
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *****************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavdevice/avdevice.h>
#ifdef __cplusplus
};
#endif

#include "common/playback_controler.h"

#include <opencv2/opencv.hpp>

//#define PERFORMANCE_COUNTER

#ifdef PERFORMANCE_COUNTER
#include <windows.h>
#endif // PERFORMANCE_COUNTER

int main(int argc, char *argv[])
{
  argc;
  argv;
#ifdef PERFORMANCE_COUNTER
  LARGE_INTEGER start;
  LARGE_INTEGER end;
  LARGE_INTEGER freq;
  double        perf_time;
#endif // PERFORMANCE_COUNTER

  bool flag = true;

  cv::namedWindow ("test", 0);
  cv::resizeWindow("test", 752, 480);

  PlaybackControler plc;

  AVDictionary    *options = NULL;
//  av_dict_set(&options, "video_size", "752x480", 0);
//  av_dict_set(&options, "auto_exposure", "true", 0);

  if (!plc.Open("video=MT9V034", options))
  {
    return -1;
  }

  int     frame_height = plc.GetHeight();
  int     frame_width  = plc.GetWidth();
  int     frame_size   = frame_height * frame_width;
  cv::Mat frame_original(frame_height,  frame_width, CV_16UC1);
  cv::Mat frame_gray    (frame_height,  frame_width, CV_8UC1 );

  do
  {
#ifdef PERFORMANCE_COUNTER
    QueryPerformanceFrequency(&freq);
    QueryPerformanceCounter(&start);
#endif // PERFORMANCE_COUNTER
    if (!plc.Read())
    {
      break;
    }

    uint8_t **bufs = plc.GetFrameYUV420();
    memcpy(frame_original.data, bufs[0], 2 * frame_size);

    for (int index = 0; index < frame_size; ++index)
    {
      frame_gray.at<uchar>(index) =
        frame_original.at<ushort>(index) >> 2;
    }

    cv::equalizeHist(frame_gray, frame_gray);

    cv::imshow("test", frame_gray);

#ifdef PERFORMANCE_COUNTER
    QueryPerformanceCounter(&end);
    perf_time =
      (double)(end.QuadPart - start.QuadPart) / (double)freq.QuadPart;
    fprintf(stderr, "Time for one frame: %.10f ms\n", 1000 * perf_time);
#endif // PERFORMANCE_COUNTER

    int key = cv::waitKey(30);
    if (key > 0)
    {
      cv::waitKey(0);
    }
  } while (flag);

  return 0;
}
