/*****************************************************************************
 *
 * Copyright (c) 2015-2016, PoeticFlower Studio.
 *
 * AUTHOR  : Peixuan Zhang <Zhang@PoeticFlower.CN>
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *****************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavdevice/avdevice.h>
#ifdef __cplusplus
};
#endif

#include "common/playback_controler.h"

PlaybackControler::PlaybackControler()
  : m_dst_video_format(AV_PIX_FMT_YUYV422)
{
  av_register_all();
  avdevice_register_all();
  avformat_network_init();
}

bool PlaybackControler::Open(const char* input_path, AVDictionary *options)
{
  AVCodec         *av_codec_video;
  AVInputFormat   *ifmt = av_find_input_format("dshow");

  m_av_format_ctx = avformat_alloc_context();

  if (0 != avformat_open_input(&m_av_format_ctx, input_path, ifmt, &options))
  {
    fprintf(stderr, "Error: Couldn't open input stream.\n");
    return false;
  }

  if (avformat_find_stream_info(m_av_format_ctx, NULL) < 0)
  {
    fprintf(stderr, "Error: Couldn't find stream information.\n");
    return false;
  }

  m_stream_index_video = -1;
  for (unsigned int i = 0; i < m_av_format_ctx->nb_streams; ++i)
  {
    if (
      AVMEDIA_TYPE_VIDEO == m_av_format_ctx->streams[i]->codec->codec_type)
    {
      m_stream_index_video = m_av_format_ctx->streams[i]->index;
      break;
    }
  }
  if (-1 == m_stream_index_video)
  {
    fprintf(stderr, "Error: Didn't find a video stream.\n");
    return false;
  }

  m_av_codec_ctx_video =
    m_av_format_ctx->streams[m_stream_index_video]->codec;
  av_codec_video = avcodec_find_decoder(m_av_codec_ctx_video->codec_id);
  if (NULL == av_codec_video)
  {
    fprintf(stderr, "Error: Video Codec not found.\n");
    return false;
  }

  if (avcodec_open2(m_av_codec_ctx_video, av_codec_video, NULL) < 0)
  {
    fprintf(stderr, "Error: Could not open video codec.\n");
    return false;
  }

  m_sws_ctx_image_convert = sws_getContext(
    m_av_codec_ctx_video->coded_width, m_av_codec_ctx_video->coded_height,
    m_av_codec_ctx_video->pix_fmt,
    m_av_codec_ctx_video->coded_width, m_av_codec_ctx_video->coded_height,
    m_dst_video_format,
    SWS_BICUBIC,
    NULL,
    NULL,
    NULL);

  if (NULL == m_sws_ctx_image_convert)
  {
    fprintf(stderr, "Error: Could not create the SwsContext.\n");
    return false;
  }

  m_av_packet = (AVPacket*)av_malloc(sizeof(AVPacket));
  av_init_packet(m_av_packet);

  m_av_frame_original = av_frame_alloc();
  m_av_frame_output   = av_frame_alloc();

  m_output_buffer = (uint8_t*)av_malloc(avpicture_get_size(
    m_dst_video_format,
    m_av_codec_ctx_video->width,
    m_av_codec_ctx_video->height));

  avpicture_fill(
    (AVPicture*)m_av_frame_output,
    m_output_buffer,
    m_dst_video_format,
    m_av_codec_ctx_video->width,
    m_av_codec_ctx_video->height);

  printf("--------------- File Information ----------------\n");
  av_dump_format(m_av_format_ctx, 0, input_path, 0);
  printf("-------------------------------------------------\n");

  return true;
}

bool PlaybackControler::Read()
{
  int got_picture;

  while (av_read_frame(m_av_format_ctx, m_av_packet) >= 0)
  {
    if (m_stream_index_video == m_av_packet->stream_index)
    {
      if (avcodec_decode_video2(
        m_av_codec_ctx_video,
        m_av_frame_original,
        &got_picture,
        m_av_packet) < 0)
      {
        fprintf(stderr, "Error: Could not decode video.\n");
        return false;
      }
      if (got_picture)
      {
        if (m_dst_video_format != m_av_codec_ctx_video->pix_fmt)
        {
          sws_scale(
            m_sws_ctx_image_convert,
            m_av_frame_original->data,
            m_av_frame_original->linesize,
            0,
            m_av_frame_original->height,
            m_av_frame_output->data,
            m_av_frame_output->linesize);
        }
        return true;
      }
    }
  }

  return false;
}

uint8_t** PlaybackControler::GetFrameYUV420()
{
  if (m_dst_video_format != m_av_codec_ctx_video->pix_fmt)
  {
    return m_av_frame_output->data;
  }

  return m_av_frame_original->data;
}
