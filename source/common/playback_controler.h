/*****************************************************************************
 *
 * Copyright (c) 2015-2016, PoeticFlower Studio.
 *
 * AUTHOR  : Peixuan Zhang <Zhang@PoeticFlower.CN>
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *****************************************************************************/

class PlaybackControler
{
public:
  PlaybackControler();
  ~PlaybackControler()
  {
  }

  bool      Open(const char* input_path, AVDictionary *options);
  bool      Read();
  uint8_t** GetFrameYUV420();
  int GetWidth()
  {
    return m_av_codec_ctx_video->coded_width;
  }
  int GetHeight()
  {
    return m_av_codec_ctx_video->coded_height;
  }
  void Release();

private:
  AVFormatContext       *m_av_format_ctx;
  AVPacket              *m_av_packet;
  AVCodecContext        *m_av_codec_ctx_video;
  const AVPixelFormat    m_dst_video_format;
  int                    m_stream_index_video;
  struct SwsContext     *m_sws_ctx_image_convert;
  AVFrame               *m_av_frame_original;
  AVFrame               *m_av_frame_output;
  uint8_t               *m_output_buffer;
};
